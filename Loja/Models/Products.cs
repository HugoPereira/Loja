﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Loja.Models
{
    public class Products
    {
        //code first = crio a bd por codigo
        //ao criar crio este campo como chave primaria
        [Key]
        public int ProductId { get; set; }

        public string Description { get; set; }

        public decimal Price { get; set; }

        public DateTime LastBuy { get; set; }

        public float Stocks{ get; set; }

        public string Remarks { get; set; }
    }
}
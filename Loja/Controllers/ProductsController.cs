﻿using Loja.Context;
using Loja.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Loja.Controllers
{
    public class ProductsController : Controller
    {
        //buscar a nossa base de dados por entity
        private StoreContext db = new StoreContext();

        // GET: Products
        public ActionResult Index()
        {
            //tenho que converter para lista, pq nao vem do linq
            return View(db.Products.ToList());
        }

        // GET: Products/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //procura na bd dos produtos este id
            var product = db.Products.Find(id);

            if (product == null)
            {
                return HttpNotFound();
            }

            return View(product);
        }

        // GET: Products/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Products/Create
        [HttpPost]
        public ActionResult Create(Products product)
        {
            try
            {
                //se for valido
                if (ModelState.IsValid)
                {
                    //adicionamos o produto a bd
                    db.Products.Add(product);
                    //guardamos
                    db.SaveChanges();

                    return RedirectToAction("Index");
                }

                return View(product);
            }
            catch
            {
                return View(product);
            }
        }

        // GET: Products/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var product = db.Products.Find(id);

            if (product == null)
            {
                return HttpNotFound();
            }

            return View(product);
        }

        // POST: Products/Edit/5
        [HttpPost]
        public ActionResult Edit(Products product)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(product).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                return View(product);
            }
            catch
            {
                return View();
            }
        }

        // GET: Products/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var product = db.Products.Find(id);

            if (product == null)
            {
                return HttpNotFound();
            }

            return View(product);
        }

        // POST: Products/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, Products product)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    product = db.Products.Find(id);

                    if (product == null)
                    {
                        return HttpNotFound();
                    }

                    db.Products.Remove(product);
                    db.SaveChanges();

                    return RedirectToAction("Index");
                }

                return View(product);
            }
            catch
            {
                return View(product);
            }
        }
    }
}

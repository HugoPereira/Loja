﻿using Loja.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Loja.Context
{
    //o storecontext vai herdar do DBContext do using entity framework
    public class StoreContext: DbContext
    {
        public DbSet<Products> Products { get; set; }

    }
}